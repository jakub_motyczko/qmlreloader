# QML Reloader

The purpose of this project is to deliver QML plugin for Reloader component which will monitor for the file changes & will reload component whenever file changes.

## Qt Version

This library was implemented using Qt 5.11.3 as this is the version I'm using on some huge project and I needed Reloader for it.

## Demo video

https://www.youtube.com/watch?v=NYHshsX_qdc

## Why do I need this?

Often when working on UI and adjusting it, you change something in QML or JS, recompile & run again. Then you change some other parameter and go all over again. 

With QMLReloader you can load your component dynamically so it's updating automatically without a need to reload/restart whole application.

## But...there's already QML Preview since Qt 5.15

Right, but it has some flaws:

- it's available since Qt 5.15 so if you're working on a project with older Qt then you simply can't use it
- it's reloading whole QML engine, by closing & opening window again. 

QMLReloader is reloading only the component that you load with it without any interruption in application.

## How does it work?

The absolute usage is just providing `url` property with absolute path of the file, so Reloader knows which file to monitor & load.

```
Reloader {
    Layout.fillWidth: true
    Layout.preferredHeight: 70

    url: "/absolute/path/to/your/component"
}
```

There are also other features Reloader has, please check out comments in Reloader.qml. Each property in there is documented.

## Ok, but what's the license?

BSD - do whatever you want with it. 

Please ping me whenever you have any idea on how to improve it, fix or maybe add new features. I'm open to any cooperation.

## What are the limitations?

Reloader won't work with remote deployment, so unfortunately you can't deploy to ARM or Android with it. It's possible to add simple client-server that will send out updated QML files to the remote host and then reload on target platform.

## How to build it?

Use QtCreator or run qmake from terminal

## How to install it?

Run `make install`

If you're using Qt that has been installed e.g. in `/opt`, then run following command: (it can vary depends on what system you have and/or where you installed your Qt)

`sudo cp -r /usr/lib/qt/qml/Reloader /opt/Qt5.11.1/5.11.1/gcc_64/qml/`