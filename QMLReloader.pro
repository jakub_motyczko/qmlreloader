TEMPLATE = subdirs

SUBDIRS += Example
SUBDIRS += Plugin
SUBDIRS += UnitTests
 
# build the project sequentially as listed in SUBDIRS
CONFIG += ordered

OTHER_FILES += \
    README.md \
