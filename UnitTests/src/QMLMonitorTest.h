#ifndef QMLMONITORTEST_H
#define QMLMONITORTEST_H

#include <QObject>


class QMLMonitorTest : public QObject
{
    Q_OBJECT

public:
    QMLMonitorTest();
    ~QMLMonitorTest();

private slots:
    void initTestCase();
    void cleanupTestCase();
    void test_getQmlDependencies_data();
    void test_getQmlDependencies();

};

#endif // QMLMONITORTEST_H
