#include "QMLMonitorTest.h"

#include <QmlReloader.h>
#include <QtTest>

// add necessary includes here



QMLMonitorTest::QMLMonitorTest()
{

}

QMLMonitorTest::~QMLMonitorTest()
{

}

void QMLMonitorTest::initTestCase()
{

}

void QMLMonitorTest::cleanupTestCase()
{

}

void QMLMonitorTest::test_getQmlDependencies_data()
{
    QTest::addColumn<QString>("fileName");
    QTest::addColumn<QStringList>("expectedResult");

    QTest::addRow("TestControl1.qml") << "/home/kuba/workspace/QMLReloader/UnitTests/resources/Components/Controls/TestControl1.qml"
                              << QStringList({
                                                 "/home/kuba/workspace/QMLReloader/UnitTests/resources/OtherComponents/OtherTestComponent1.qml"
                                             });

    QTest::addRow("TestControl2.qml") << "/home/kuba/workspace/QMLReloader/UnitTests/resources/Components/Controls/TestControl2.qml"
                              << QStringList({
                                                 "/home/kuba/workspace/QMLReloader/UnitTests/resources/Components/TestComponent3.qml"
                                                 , "/home/kuba/workspace/QMLReloader/UnitTests/resources/OtherComponents/OtherTestComponent2.qml"
                                             });

    QTest::addRow("TestComponent1.qml") << "/home/kuba/workspace/QMLReloader/UnitTests/resources/Components/TestComponent1.qml"
                              << QStringList({
                                                  "/home/kuba/workspace/QMLReloader/UnitTests/resources/Components/Controls/TestControl1.qml"
                                                 ,"/home/kuba/workspace/QMLReloader/UnitTests/resources/OtherComponents/OtherTestComponent1.qml"
                                             });

    QTest::addRow("TestComponent2.qml") << "/home/kuba/workspace/QMLReloader/UnitTests/resources/Components/TestComponent2.qml"
                              << QStringList({
                                                  "/home/kuba/workspace/QMLReloader/UnitTests/resources/Components/Controls/TestControl2.qml"
                                                 ,"/home/kuba/workspace/QMLReloader/UnitTests/resources/Components/TestComponent3.qml"
                                                 , "/home/kuba/workspace/QMLReloader/UnitTests/resources/OtherComponents/OtherTestComponent2.qml"
                                             });

    QTest::addRow("TestComponent3.qml") << "/home/kuba/workspace/QMLReloader/UnitTests/resources/Components/TestComponent3.qml"
                              << QStringList({ });

    QTest::addRow("Page.qml") << "/home/kuba/workspace/QMLReloader/UnitTests/resources/Page.qml"
                              << QStringList({
                                                   "/home/kuba/workspace/QMLReloader/UnitTests/resources/Components/TestComponent1.qml"
                                                 , "/home/kuba/workspace/QMLReloader/UnitTests/resources/Components/Controls/TestControl1.qml"
                                                 , "/home/kuba/workspace/QMLReloader/UnitTests/resources/OtherComponents/OtherTestComponent1.qml"
                                                 , "/home/kuba/workspace/QMLReloader/UnitTests/resources/Components/TestComponent2.qml"
                                                 , "/home/kuba/workspace/QMLReloader/UnitTests/resources/Components/Controls/TestControl2.qml"
                                                 , "/home/kuba/workspace/QMLReloader/UnitTests/resources/Components/TestComponent3.qml"
                                                 , "/home/kuba/workspace/QMLReloader/UnitTests/resources/OtherComponents/OtherTestComponent2.qml"
                                             });

}

void QMLMonitorTest::test_getQmlDependencies()
{
    QFETCH(QString, fileName);
    QFETCH(QStringList, expectedResult);

    const auto &result = QmlMonitor::getQmlDependencies(fileName);

    qDebug() << result;
    QCOMPARE(result, expectedResult);
}

QTEST_MAIN(QMLMonitorTest)
