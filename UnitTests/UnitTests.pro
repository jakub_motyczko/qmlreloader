QT += testlib quick
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

include($$PWD/../Core/Core.pri)

SOURCES += \
    src/QMLMonitorTest.cpp
HEADERS += \
    src/QMLMonitorTest.h

RESOURCES += \
    resources/qml.qrc

INCLUDEPATH += $$PWD/../Core/src/
