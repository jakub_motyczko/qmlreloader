QT += quick

CONFIG += c++11

SOURCES += \
        $$PWD/src/QmlMonitor.cpp \
        $$PWD/src/QmlReloader.cpp

HEADERS += \
        $$PWD/src/QmlMonitor.h \
        $$PWD/src/QmlReloader.h

INCLUDEPATH += $$PWD/src/
