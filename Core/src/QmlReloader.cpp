#include "QmlReloader.h"

#include <QDebug>
#include <QQmlApplicationEngine>
#include <QQmlComponent>
#include <QQmlContext>
#include <QQuickItem>

QmlReloader::QmlReloader(QObject *parent) : QObject(parent)
{
    initMetaData();

    connect(&mQmlMonitor, &QmlMonitor::changed, this, [this](){
        emit changed();

        load();
    });
}

void QmlReloader::initMetaData()
{
    static bool initialized = false;
    if (initialized) {
        return;
    }
    initialized = true;
}

void QmlReloader::load()
{
    if (mLoader == nullptr) {
        qWarning() << "Loader is null";
        return;
    }

    if (mEngine == nullptr) {
        auto context = QQmlApplicationEngine::contextForObject(mLoader);
        if (context == nullptr) {
            qWarning() << "No context for Loader object";
            return;
        }
        mEngine = context->engine();
    }

    if (mEngine == nullptr) {
        qWarning() << "failed to retrieve QQmlEngine.";
        return;
    }
    mEngine->clearComponentCache();
    mQmlMonitor.setUrl(mUrl);
    mQmlMonitor.refresh();

    emit readyToLoad();

//TODO figure out how to implement QQmlComponent::createWithInitialProperties in Qt 5.11 (it's available from 5.14)
// for now component creation is handled in QML
//    mLoader->setProperty("active", false);
//    mLoader->setProperty("source", mUrl);
//    mLoader->setProperty("active", true);

//    if (mParentItem != nullptr && !mItemId.isEmpty()) {
//        mParentItem->setProperty("homePage", mLoader->property("item"));
//    }

//    qDebug() << mLoader << mLoader->property("source") << mLoader->property("item");

}
