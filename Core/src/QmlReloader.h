#ifndef QMLRELOADER_H
#define QMLRELOADER_H

#include <QJSValue>
#include <QObject>

#include "QmlMonitor.h"

class QQmlEngine;

class QQmlV4Function;

class QmlReloader : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QObject* loader MEMBER mLoader)
    Q_PROPERTY(QString url MEMBER mUrl)
public:
    explicit QmlReloader(QObject *parent = nullptr);

    Q_INVOKABLE void load();

private:
    QmlMonitor mQmlMonitor;
    QString mUrl;
    QObject *mLoader = nullptr;
    QQmlEngine *mEngine = nullptr;

private:
    void initMetaData();

signals:
    void changed() const;
    void readyToLoad() const;

};

#endif // QMLRELOADER_H
