#include "QmlMonitor.h"

#include <QFileInfo>
#include <QDebug>

QmlMonitor::QmlMonitor(QObject *parent) : QObject(parent)
{
    connect(&mFileWatcher, &QFileSystemWatcher::fileChanged, this, [this](const QString &/*fileName*/){
        refresh();
        emit changed();
    });
}

const QString &QmlMonitor::url() const
{
    return mUrl;
}

void QmlMonitor::setUrl(const QString &newUrl)
{
    mUrl = newUrl;
}

void QmlMonitor::refresh()
{
    if (!mFileWatcher.files().contains(mUrl))
        mFileWatcher.addPath(mUrl);

    const QStringList dependencies = getQmlDependencies(mUrl);

    for (const QString &dependency : dependencies) {
        if (!mFileWatcher.files().contains(dependency))
            mFileWatcher.addPath(dependency);
    }
    qDebug() << "URLs" << mFileWatcher.files();
}

QStringList QmlMonitor::getQmlDependencies(const QString &fileName)
{
    static int depth = 0;
    static QStringList mCircularDependencyDetectorList;

    if (depth == 0) {
        mCircularDependencyDetectorList.clear();
    }

    if (mCircularDependencyDetectorList.contains(fileName)) {
        return QStringList();
    }

    ++depth;

    const QByteArray debugPrefix(depth, '-');


    QFile file(fileName);
    if (!file.open(QFile::ReadOnly)) {
        qWarning() << "Failed to open" << fileName;
        --depth;
        return QStringList();
    }

    qDebug().noquote() << debugPrefix << "source:" << fileName;

    //parse all imports
    QVector<QByteArray> importLines;
    QVector<QString> components;
    {
        QByteArray line;
        while(!file.atEnd()) {
            line = file.readLine();
            if (line.startsWith("import \"")) {
                int bracketOffset = line.indexOf('\"');

                line.remove(0, bracketOffset + 1);

                bracketOffset = line.indexOf('\"');
                line.remove(bracketOffset, line.length() - bracketOffset);

                importLines.append(line.trimmed());
            }
            if (line.contains('{')) {
               const int bracketOffset = line.indexOf('{');
               line.remove(bracketOffset, line.length() - bracketOffset);
               components.append(line.trimmed());
            }
        }
    }

    QStringList importPaths;

    {
        const QFileInfo fileInfo(fileName);
        const QString &filePath = fileInfo.absolutePath();
        //convert import paths to absolutePaths
        for (const auto &line : qAsConst(importLines)) {

            const QFileInfo info(filePath + "/" + line);
            qDebug().noquote() << debugPrefix << "import:" << info.absoluteFilePath();
            importPaths.append(info.absoluteFilePath());
        }
    }

    QStringList result;
    //convert import paths to absolutePaths
    for (auto component : components) {
        qDebug().noquote() << debugPrefix << "component:" << component;

        for (auto path : importPaths) {
            const QFileInfo info(QStringLiteral("%1/%2.qml").arg(path).arg(component));
            if (info.exists()) {
                const QString &infoFilePath = info.absoluteFilePath();
                qDebug().noquote() << debugPrefix << "dependency:" << infoFilePath;
                result.append(infoFilePath);
                result.append(getQmlDependencies(infoFilePath));
            }
        }
    }

    --depth;
    return result;
}

//QString QmlReloader::getFileContents() const
//{
//    QFile file(mUrl);
//    if (file.open(QFile::ReadOnly)) {
//        return file.readAll();
//    }

//    return QString();
//}
