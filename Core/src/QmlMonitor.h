#ifndef QMLMONITOR_H
#define QMLMONITOR_H

#include <QObject>
#include <QFileSystemWatcher>

/*!
 * \brief The QmlMonitor class is responsible for monitoring QML file and it's dependencies/imports
 */
class QmlMonitor : public QObject
{
    Q_OBJECT
public:
    QmlMonitor(QObject *parent = nullptr);
    ~QmlMonitor() {}

    const QString &url() const;
    void setUrl(const QString &newUrl);

    static QStringList getQmlDependencies(const QString &fileName);

    void refresh();

private:
    QString mUrl;
    QFileSystemWatcher mFileWatcher;

    void handleSetFileName(const QString &fileName);

signals:
    void changed() const;

};

#endif // QMLMONITOR_H
