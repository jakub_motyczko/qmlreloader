TEMPLATE = lib
CONFIG += plugin
QT += qml

include($$PWD/../Core/Core.pri)

DESTDIR = Reloader
TARGET  = qmlreloaderplugin

SOURCES += plugin.cpp

#QT_INSTALL_EXAMPLES = /opt/Qt5.11.1/Examples/Qt-5.11.1/

qml.files = Reloader.qml
qml.path += /usr/lib/qt/qml/Reloader

target.path += /usr/lib/qt/qml//Reloader

pluginfiles.files = qmldir
pluginfiles.path += /usr/lib/qt/qml//Reloader

INSTALLS += target qml pluginfiles

CONFIG += install_ok
