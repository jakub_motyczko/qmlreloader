#include <QtQml/QQmlExtensionPlugin>
#include <QtQml/qqml.h>

#include <QmlReloader.h>

//![plugin]
class ReloaderQmlPlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID QQmlExtensionInterface_iid)

public:
    void registerTypes(const char *uri) override
    {
        Q_ASSERT(uri == QLatin1String("Reloader"));
        qmlRegisterType<QmlReloader>("QmlReloader", 1, 0, "QmlReloader");
    }
};
//![plugin]

#include "plugin.moc"
