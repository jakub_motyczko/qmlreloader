#ifndef QMLRELOADER_H
#define QMLRELOADER_H

#include <QFileSystemWatcher>
#include <QObject>

class QMLReloader : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QObject* loader MEMBER mLoader)
    Q_PROPERTY(QObject* engine MEMBER mEngine)
    Q_PROPERTY(QString url MEMBER mUrl)
public:
    explicit QMLReloader(QObject *parent = nullptr);

    enum Error {
        NoError,
        InputFileNotQML,
        InputFileDoesntExist,
    };

    static QStringList getQmlDependencies(const QString &fileName);

    Q_INVOKABLE QString getFileContents() const;

    Q_INVOKABLE void load();


private:
    QString mInitialFileName;
    QFileSystemWatcher mFileWatcher;
    QString mUrl;
    QObject *mLoader = nullptr;
    QObject *mEngine = nullptr;

private:
    void initMetaData();
    void handleSetFileName(const QString &fileName);

    void setFile(const QString &fileName);

signals:
//incoming
    void setFileName(const QString &fileName) const;

//outgoing
    void changed() const;

    void error(Error error) const;

};

#endif // QMLRELOADER_H
