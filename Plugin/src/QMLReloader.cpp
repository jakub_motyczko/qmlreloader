#include "QMLReloader.h"

#include <QFileInfo>
#include <QDebug>
#include <QQmlApplicationEngine>

QMLReloader::QMLReloader(QObject *parent) : QObject(parent)
{
    initMetaData();

    connect(this, &QMLReloader::setFileName, this, &QMLReloader::handleSetFileName);

    connect(&mFileWatcher, &QFileSystemWatcher::fileChanged, this, [this](const QString &/*fileName*/){
        setFile(mUrl);

        emit changed();

        load();
    });
}

void QMLReloader::initMetaData()
{
    static bool initialized = false;
    if (initialized) {
        return;
    }
    initialized = true;

    qRegisterMetaType<QMLReloader::Error>("QMLMonitor::Error");
}

void QMLReloader::handleSetFileName(const QString &fileName)
{
    if (!fileName.endsWith(".qml")) {
        emit error(Error::InputFileNotQML);
        return;
    }

    if (!QFileInfo::exists(fileName)) {
        emit error(Error::InputFileDoesntExist);
        return;
    }

    mInitialFileName = fileName;
    setFile(fileName);
}

void QMLReloader::setFile(const QString &fileName)
{
    if (!mFileWatcher.files().contains(fileName))
        mFileWatcher.addPath(fileName);

    const QStringList dependencies = getQmlDependencies(fileName);

    for (const QString &dependency : dependencies) {
        if (!mFileWatcher.files().contains(dependency))
            mFileWatcher.addPath(dependency);
    }
    qDebug() << "URLs" << mFileWatcher.files();
}

QStringList QMLReloader::getQmlDependencies(const QString &fileName)
{
    static int depth = 0;
    static QStringList mCircularDependencyDetectorList;

    if (depth == 0) {
        mCircularDependencyDetectorList.clear();
    }

    if (mCircularDependencyDetectorList.contains(fileName)) {
        return QStringList();
    }

    ++depth;

    const QByteArray debugPrefix(depth, '-');


    QFile file(fileName);
    if (!file.open(QFile::ReadOnly)) {
        qWarning() << "Failed to open" << fileName;
        --depth;
        return QStringList();
    }

    qDebug().noquote() << debugPrefix << "source:" << fileName;

    //parse all imports
    QVector<QByteArray> importLines;
    QVector<QString> components;
    {
        QByteArray line;
        while(!file.atEnd()) {
            line = file.readLine();
            if (line.startsWith("import \"")) {
                int bracketOffset = line.indexOf('\"');

                line.remove(0, bracketOffset + 1);

                bracketOffset = line.indexOf('\"');
                line.remove(bracketOffset, line.length() - bracketOffset);

                importLines.append(line.trimmed());
            }
            if (line.contains('{')) {
               const int bracketOffset = line.indexOf('{');
               line.remove(bracketOffset, line.length() - bracketOffset);
               components.append(line.trimmed());
            }
        }
    }

    QStringList importPaths;

    {
        const QFileInfo fileInfo(fileName);
        const QString &filePath = fileInfo.absolutePath();
        //convert import paths to absolutePaths
        for (const auto &line : qAsConst(importLines)) {

            const QFileInfo info(filePath + "/" + line);
            qDebug().noquote() << debugPrefix << "import:" << info.absoluteFilePath();
            importPaths.append(info.absoluteFilePath());
        }
    }

    QStringList result;
    //convert import paths to absolutePaths
    for (auto component : components) {
        qDebug().noquote() << debugPrefix << "component:" << component;

        for (auto path : importPaths) {
            const QFileInfo info(QStringLiteral("%1/%2.qml").arg(path).arg(component));
            if (info.exists()) {
                const QString &infoFilePath = info.absoluteFilePath();
                qDebug().noquote() << debugPrefix << "dependency:" << infoFilePath;
                result.append(infoFilePath);
                result.append(getQmlDependencies(infoFilePath));
            }
        }
    }

    --depth;
    return result;
}

QString QMLReloader::getFileContents() const
{
    QFile file(mInitialFileName);
    if (file.open(QFile::ReadOnly)) {
        return file.readAll();
    }

    return QString();
}

void QMLReloader::load()
{
    if (mLoader == nullptr) {
        qWarning() << "Loader is null";
        return;
    }

    auto engine = qobject_cast<QQmlApplicationEngine*>(mEngine);
    if (engine == nullptr) {
        qWarning() << "engine property is not of QQmlApplicationEngine. Set contextProperty(QQmlApplicationEngine) and assign in QMLReloader";
        return;
    }
    engine->clearComponentCache();
    setFile(mUrl);

    mLoader->setProperty("active", false);
    mLoader->setProperty("source", mUrl);
    mLoader->setProperty("active", true);
    qDebug() << mLoader << mLoader->property("source");
}


