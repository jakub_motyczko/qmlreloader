import QtQuick 2.11

import QmlReloader 1.0

Item {
    //absolute path to the file with no prefix like "qrc:/" or "file:/"
    property alias url: reloader.url
    property alias source: reloader.url

    //initial item creation parameters
    property var parameters: ({ })

    //parentItem to which assign the loaded item
    property var parentItem: null

    //itemId is the property name of parentItem to which loaded item will be assigned
    property string itemId: reloader.itemId

    //loaded item shared outside
    property alias item: loader.item

    //enables autoloading of the item right after Reloader is being created. otherwise on demand load() method has to be called
    property bool autoload: true

    function load() {
        reloader.load()
    }

    Component.onCompleted: {
        if (autoload) {
            load()
        }
    }

    QmlReloader {
        id: reloader
        loader: loader

        onReadyToLoad: {
            if (url.length == 0) {
                console.warn("Reloader URL is empty. Aborting")
                return
            }
            loader.setSource("file:/" + url, parameters)
        }
    }

    Loader {
        id: loader
        anchors.fill: parent

        onItemChanged: {
            //TODO warn if missing
            if (parentItem != null && itemId.length > 0) {
                parentItem[itemId] = item
            }
        }
    }

    Rectangle {
        id: errorPopup
        visible: loader.status === Loader.Error
        anchors.fill: parent
        color: "darkred"

        Text {
            anchors.centerIn: parent
            text: qsTr("Reloader failed to load component. Check logs")
            color: "#FFFFFF"
        }
    }
}
