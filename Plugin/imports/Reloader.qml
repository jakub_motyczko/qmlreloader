import QtQuick 2.11

import QMLReloader 1.0

//this item requires QQmlApplicationEngine passed to QML as qmlEngine root context property
Item {
    property alias url: reloader.url
    property alias source: reloader.url

    function load() {
        reloader.load()
    }

    QMLReloader {
        id: reloader
        loader: loader
        engine: qmlEngine
    }

    Loader {
        id: loader
        anchors.fill: parent
    }
}
