import QtQuick 2.11
import QtQuick.Window 2.11
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

import Reloader 1.0

import "Components"

Window {
    id: mainWindow
    width: 640
    height: 720
    visible: true
    title: qsTr("Reloader example")

    //property for handling reloader item
    property var counter

    property int counterTicks: counter.ticks
    property int itemTicks: reloader.item.ticks

    QtObject {
        id: handler

        function handle() {
            console.error("handled inside Reloader")
        }
    }

    function exampleConnection(text) {
        print("exampleConnection: %1".arg(text))
    }


    onCounterTicksChanged: print("counter.ticks:" + counterTicks)
    onItemTicksChanged: print("itemTicks:" + itemTicks)

    Connections {
        target: reloader.item

        onExampleSignal: exampleConnection(text)
    }

    ColumnLayout {
        anchors.fill: parent

        TitleBar {
            Layout.fillWidth: true
            Layout.preferredHeight: 70
        }

        Reloader {
            Layout.fillWidth: true
            Layout.preferredHeight: 70

            //please change this path to your local path when running this example
            url: "/home/kuba/workspace/QMLReloader/Example/src/qml/Components/TitleBar.qml"
        }

        Reloader {
            id: reloader
            Layout.fillWidth: true
            Layout.fillHeight: true

            //please change this path to your local path when running this example
            url: "/home/kuba/workspace/QMLReloader/Example/src/qml/Components/Counter.qml"
            parameters: { "ticks" : 10, "handler" : handler }
            parentItem: mainWindow
            itemId: "counter"
        }
    }
}
