import QtQuick 2.11
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.0

ToolBar {
    id: titleBar

    height: 70
    leftPadding: 16
    rightPadding: 0
    Material.foreground: "#FAFAFA"

    Material.background: Material.Blue
//    Material.background: Material.Red
//    Material.background: Material.Orange

    Label {
        id: toolbarTitle

        anchors.fill: parent
        text: "Reloader Example"

        font.pixelSize: 24
        elide: Label.ElideRight
        horizontalAlignment: Qt.AlignLeft
        verticalAlignment: Qt.AlignVCenter
    }
}
