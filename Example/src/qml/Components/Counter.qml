import QtQuick 2.11
import QtQuick.Controls 2.2

import "Controls"

//change this file when running app to check if refresh will work (you need to save the file)
Rectangle {
//    color: "red"
    color: "white"

    property int ticks: 0
    property var handler: null

    signal exampleSignal(var text)

    function handleMouseAreaClick() {
        if (handler != null) {
            handler.handle()
        }
        exampleSignal("example value")
    }

    Timer {
        interval: 1000
        repeat: true
        running: true
        onTriggered: ++ticks
    }

    ExampleControl {
        anchors {
            fill: parent
            margins: parent.width / 10
        }

        Text {
            anchors.centerIn: parent
            text: ticks
            color: "white"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter

            font {
                pixelSize: parent.height / 2
                weight: Font.Bold
                family: "Helvetica"
            }

            Component.onCompleted: {
                print("text completed")
            }
        }
    }

    MouseArea {
        anchors.fill: parent

        onClicked: handleMouseAreaClick()
    }
}
