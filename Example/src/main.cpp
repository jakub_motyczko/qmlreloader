#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QStandardPaths>
#include <QDebug>

#include <QmlReloader.h>

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif
    qSetMessagePattern("[%{time yyyy-MM-dd h:mm:ss.zzz} [%{if-debug}D%{endif}%{if-info}I%{endif}%{if-warning}W%{endif}%{if-critical}C%{endif}%{if-fatal}F%{endif}]]\t%{qthreadptr}\t%{file}:%{line}\t%{function}\n\t\t\t%{message}");

    QGuiApplication app(argc, argv);

    qDebug() << QStandardPaths::writableLocation(QStandardPaths::CacheLocation);

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);

//    qmlRegisterType<QmlReloader>("QmlReloaderNoPlugin", 1, 0, "QmlReloader");

    engine.load(url);

    return app.exec();
}
